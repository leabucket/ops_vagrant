#!/usr/bin/env bash

## This will bootstrap puppet on the Ubuntu vagrant box

set -e

## Load the machine relative infos
. /etc/lsb-release

## Puppet version installed is 4.3.x

REPO_DEB_URL="http://apt.puppetlabs.com/puppetlabs-release-pc1-${DISTRIB_CODENAME}.deb"
echo "===> REPO URL: ${REPO_DEB_URL}"

##make sure person installing is a Root user

if [ "$(id -u)" != "0" ]; then
  echo "This script should be run as root." >&2
  exit 1
fi

if [[ -x /opt/puppetlabs/puppet/bin/puppet ]]; then
  echo "Puppet is already installed"
  exit 0
fi

## if puppet is not installed, Do install puppet
echo "===> INFO: Initial apt-get update..."
apt-get update > /dev/null

echo "===> INFO: Installing wget..."
apt-get install -y wget > /dev/null

echo "===> INFO: Installing the puppetlabs repo..."
repo_deb_path=$(mktemp)
wget --output-document="${repo_deb_path}" "REPO_DEB_URL" 2>/dev/null
dpkg --force-confdef --force-confnew -i "${repo_deb_path}" >/dev/null
apt-get update >/dev/null


echo "===> INFO: Now installing Puppet..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o  Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install puppet-agent >/dev/null
echo "DONE. Puppet Installed!"
