#!/usr/bin/env bash

if [ "$(ruby --version | grep 'ruby 1.8.7')" ]
then
	apt-get install ruby1.9.1 --fix-missing --assume-yes
	update-alternatives --set ruby /usr/bin/ruby1.9.1
	apt-get install libaugeas-ruby  --fix-missing --assume-yes
	#apt-get install ruby1.9.1-dev  --fix-missing --assume-yes
	#gem install ruby-augeas
fi;
